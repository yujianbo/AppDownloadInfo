package com.ssca.mysql;

public class AppDownload {

	private String url;

	private int download_floor;

	private String category;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getDownload_floor() {
		return download_floor;
	}

	public void setDownload_floor(int download_floor) {
		this.download_floor = download_floor;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
