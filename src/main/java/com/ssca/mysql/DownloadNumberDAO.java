package com.ssca.mysql;

import java.util.List;

public interface DownloadNumberDAO {
	public List<AppDownload> getDownloadNumber() throws Exception;
}
