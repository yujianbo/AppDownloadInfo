package com.ssca.mysql.category;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InitialUtils {

	static Logger logger = LogManager.getLogger();

	@SuppressWarnings("rawtypes")
	public static Map<String, String> initial(Map<String, String> packageCategoryMap) {
		Map<String, String> categoryMap = new HashMap<String, String>();
		Iterator iter = packageCategoryMap.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();
			String tmpCategory = jduge(value);
			if (tmpCategory != null) {
				categoryMap.put(key, tmpCategory);
			} else {
				logger.info(value);
			}
		}
		return categoryMap;
	}

	private static String jduge(String str) {
		String returnStrings = null;
		switch (str) {

		case "天气":
		case "个性化":
		case "医药":
		case "保健与健身":
		case "Weather":
		case "旅游与本地出行":
		case "Health & Fitness":
		case "Travel & Local":
		case "Arcade":
		case "交通":
		case "Transport":
		case "财经":
		case "生活方式":
		case "Personalisation":
		case "Lifestyle":
		case "Libraries & Demo":
		case "Books & Reference":
		case "Trivia":
			returnStrings = "生活";
			break;
			
		case "Education":
		case "Educational":
		case "教育":
			returnStrings = "教育";
			break;

		case "软件与演示":
		case "图书与工具书":
		case "商务":
		case "公司":
		case "Word":
		case "Board":
			returnStrings = "办公";
			break;

		case "动漫":
		case "新闻杂志":
		case "生活时尚":
		case "摄影":
		case "媒体与视频":
		case "音乐与音频":
		case "体育":
		case "娱乐":
		case "Sports":
		case "Music":
		case "Comics":
		case "比赛":
		case "精彩娱乐内容":
		case "Simulation":
		case "Casual":
		case "Puzzle":
		case "News & Magazines":
		case "Media & Video":
		case "Music & Audio":
		case "Casino":
		case "Entertainment":
			returnStrings = "娱乐";
			break;

		case "通讯":
		case "通信":
		case "Communication":
		case "社交":
		case "Social":
			returnStrings = "社交";
			break;

		case "游戏":
		case "赛车游戏":
		case "休闲游戏":
		case "体育游戏":
		case "纸牌游戏":
		case "益智类游戏":
		case "益智和解谜游戏":
		case "Adventure":
		case "Racing":
		case "Action":
		case "Card":
		case "Strategy":
		case "Role Playing":
		case "街机和动作游戏":
			returnStrings = "游戏";
			break;

		case "购物":
		case "Shopping":
		case "Business":
		case "Finance":
		case "Productivity":
			returnStrings = "商务";
			break;

		case "工具":
		case "Tools":
		case "效率":
			returnStrings = "工具";
			break;

		default:
			break;
		}
		return returnStrings;
	}

}
