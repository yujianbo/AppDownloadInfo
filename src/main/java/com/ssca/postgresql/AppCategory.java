package com.ssca.postgresql;

public class AppCategory {

	private int id;

	private String app_category;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApp_category() {
		return app_category;
	}

	public void setApp_category(String app_category) {
		this.app_category = app_category;
	}
}
