package com.ssca.postgresql;

public class AppInfo {

	private int id;

	private String app_pkgname;

	private int download_number;

	private int app_category_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApp_pkgname() {
		return app_pkgname;
	}

	public void setApp_pkgname(String app_pkgname) {
		this.app_pkgname = app_pkgname;
	}

	public int getDownload_number() {
		return download_number;
	}

	public void setDownload_number(int download_number) {
		this.download_number = download_number;
	}

	public int getApp_category_id() {
		return app_category_id;
	}

	public void setApp_category_id(int app_category_id) {
		this.app_category_id = app_category_id;
	}

}
