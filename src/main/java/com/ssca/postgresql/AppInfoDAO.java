package com.ssca.postgresql;

import org.apache.ibatis.annotations.Param;

public interface AppInfoDAO {

	public Integer selectAppInfo(String packageName) throws Exception;

	public void updateAppInfo(@Param("packageName") String packageName, @Param("downloadNumber") Integer downloadNumber,
			@Param("app_category_id") Integer app_category_id) throws Exception;

	public void insertAppInfo(@Param("packageName") String packageName, @Param("downloadNumber") Integer downloadNumber,
			@Param("app_category_id") Integer app_category_id) throws Exception;

}
