package com.ssca.postgresql;

import org.apache.ibatis.annotations.Param;

public interface AppCategoryDAO {

	public Integer selectAppCategoryID(@Param("categoryName") String categoryName) throws Exception;

	public Integer insertAppCategory(@Param("categoryName") String categoryName) throws Exception;

}
