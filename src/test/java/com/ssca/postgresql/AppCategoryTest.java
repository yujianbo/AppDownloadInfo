package com.ssca.postgresql;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class AppCategoryTest {

	Logger logger = LogManager.getLogger();
	
	@Test
	public void test() {
		String c = "测试";
		Reader reader;
		try {
			reader = Resources.getResourceAsReader("MybatisConfig.xml");
			SqlSessionFactoryBuilder ssfBuilder = new SqlSessionFactoryBuilder();
			SqlSessionFactory sqlSessionFactory = ssfBuilder.build(reader, "postgresql");
			SqlSession sqlSession = sqlSessionFactory.openSession();
			AppCategoryDAO appCategory = sqlSession.getMapper(AppCategoryDAO.class);
			Integer number = appCategory.selectAppCategoryID(c);
			logger.info("" + number);
			if (number == null) {
				appCategory.insertAppCategory(c);
				logger.info("" + appCategory.selectAppCategoryID(c));
			}
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
